class Horse(val name: String) {
    override fun toString(): String
    {
        return "|-$name-|/"
    }
}

fun run(){
    println("$this -> $this")
}

fun neigh(){
    
}

fun main() {
    println("Horse")
    var horse = Horse("Beauty")
    var horse2 = Horse("Black Beauty")
    println("$horse $horse2")
    run()
    neigh()
}